// Eddibliothèque 
// CARTOUCHE fichier JavaScript
// Auteur : Eddie THIEBAUT ( 26/11/2020)

const modal = document.querySelector('.site-container'),
    valider = document.querySelector('.valider'),
    login = document.querySelector('#login'),
    password = document.querySelector('#password'),
    popup = document.querySelector('.sign-in'),
    search = document.querySelector('#search'),
    resetBtn = document.querySelector('#search-reset-btn'),
    resetIcon = document.querySelector('.search-reset-icon');

let user = document.querySelector('#user'),
    panier = document.querySelector('#panier'),
    codePromo = document.querySelector('#code-promo');

modal.classList.add("modal") // Ajout de modal au container

// Identifiant
const loginScript = function () {
    let log = login.value.toLowerCase(),
        pass = password.value;

    if ((log === pass && (log !== "")) || (log === "guest") || (log === "admin" && pass === "secret")) {
        let hidden = function () {
            popup.classList.toggle("hidden"); //Passe la fenetre de login en display none
            modal.classList.remove("modal") //Retire .modal au container principal
            // document.querySelector('.site-content').style.overflowY = "scroll"
        }
        if (log === "guest") {
            popup.innerHTML = 'Connecté en tant qu\'invité'
            user.innerHTML = log.toUpperCase()
            panier.parentNode.removeChild(panier) //on retire le panier pour défaut de connexion
        } else {
            popup.innerHTML = `Connecté`
            user.innerHTML = log.toUpperCase()
        }
       setTimeout(hidden, 1000); // Fonction hidden après 1 seconde
    } else if (log === "" || pass === "") {
        popup.innerHTML += '<div class="sign-in" id="tempClass"> Merci d\'entrer votre<br> Identifiant et mot de passe/div>';
        document.querySelector('#tempClass').innerHTML += '<div class="btn reloadClass">ok</div>';
        document.querySelector('.reloadClass').addEventListener('click', reRun);
    } else {
        popup.innerHTML += '<div class="sign-in" id="tempClass"> Identifiant ou mot de passe incorrect </div>';
        document.querySelector('#tempClass').innerHTML += '<div class="btn reloadClass">ok</div>';
        document.querySelector('.reloadClass').addEventListener('click', reRun);
    }

}

valider.addEventListener('click', loginScript)

function reRun() {
    window.location.reload();
}



/*choix au pif d'un album pour mettre en fond


albums.forEach(album => {
    let serie = series.get(album.idSerie),
        nomFic = `${serie.nom}-${album.numero}-${album.titre}`;
    nomFic = nomFic.replace(/['!?.":$]/g, "")
    randomBG.push(`${srcAlbum}${nomFic}.jpg`)
});

randomNumber = Math.floor(Math.random() * randomBG.length);

let style = document.createElement("style");
document.head.appendChild(style)
style.sheet.insertRule(`#main-title-bg {background-image: linear-gradient(to bottom, #000, #0008), url("${randomBG[randomNumber]}")}`);*/

// Bouton reset de recherche toggle class
function resetFunction() {
    if (search.value.length !== 0) {
        resetIcon.classList.add('search-reset-icon-active')
    } else {
        resetIcon.classList.remove('search-reset-icon-active')
    }
}

search.addEventListener('input', resetFunction);

resetBtn.addEventListener('click', function () {
    resetIcon.classList.remove('search-reset-icon-active')
});

// Generation Code Promo
const GenCodePromo = new Date(),
    options = {weekday: 'short'};
codePromo.innerHTML = `${GenCodePromo.toLocaleDateString('fr-FR', options).replace(/[.]/, '-')}BF20`;


// Remplissage DropDown Combo et autre fonction associé

const listAuteur = document.querySelector('#dropdown-auteur-content'),
    listSerie = document.querySelector('#dropdown-serie-content'),
    listAlbum = document.querySelector('#dropdown-album-content'),
    listResult = document.querySelector('#result-box');


auteurs.forEach(auteur => {                                     // On genere la liste des auteurs
    let li = document.createElement('li');
    listAuteur.appendChild(li).innerText = auteur.nom
});

const selectAuteur = document.querySelectorAll('#dropdown-auteur li'),
    txtAuteur = document.querySelector('.auteur');

// On genere la liste des albums/serie au choix d'un auteur

selectAuteur.forEach(item => {          // On sélectionne un auteur au click
    let idAuteurs = 0,
        idSeries = [];
    item.addEventListener("click", function () {
        txtAuteur.innerHTML = this.innerHTML
        auteurs.forEach((e, key) => {
            if (this.innerHTML === e.nom) {
                idAuteurs = key
                albums.forEach(e => {
                    if (idAuteurs === e.idAuteur) {
                        if (idSeries.indexOf(e.idSerie) === -1) {
                            idSeries.push(e.idSerie)
                        }
                    }
                })
            }
        })
        // on supprime les champs si un auteur avait deja été choisi
        if ((listAlbum.innerHTML.length !== 0) || (listSerie.innerHTML.length !== 0)) {
            listAlbum.querySelectorAll('*').forEach(n => n.remove());
            listSerie.querySelectorAll('*').forEach(n => n.remove());
            listResult.querySelectorAll('*').forEach(n => n.remove());
        }
        // On genere la liste des serie associer à l'auteur
        series.forEach((e, key) => {
            idSeries.forEach(elem => {
                if (key === elem) {
                    let li = document.createElement('li');
                    listSerie.appendChild(li).innerText = e.nom
                }
            })

        })

        // On genere la liste des albums
        albums.forEach(e => {
            if (e.idAuteur === idAuteurs) {
                let li = document.createElement('li');
                listAlbum.appendChild(li).innerText = e.titre


                let serie = series.get(e.idSerie),
                    nomFic = `${serie.nom}-${e.numero}-${e.titre}`;
                nomFic = nomFic.replace(/['!?.":$]/g, "")


                // generation des box
                let div = document.createElement('div')
                listResult.appendChild(div).innerHTML = `  
             
                
                <h3 class="result-title-bd">${e.titre}</h3>
                <section class="result-info">
                <img class='result-img' src="${srcAlbumMini}${nomFic}.jpg" alt="cover BD">
                <div class="result-txt">
                <h4>Auteur :</h4> 
                <p class="result-auteur">${txtAuteur.innerHTML}</p>
                <h4 class="result-prix">Prix : ${e.prix}€</h4>
                </div>
                </section>
                `

            }
        })
    })
})
;


// jQuery
(function ($) {
    // menu responsive
    $('#header-icon').click(function (e) {
        e.preventDefault();
        $('body').toggleClass('with--sidebar');
    });
    $('#site-cache').click(function () {
        $('body').removeClass('with--sidebar');
    })
    // barre de recherche responsive
    $('#search-icon-mobile').click(function (e) {
        e.preventDefault();
        $('.search-collapse').toggleClass('search-dropdown');
    });
    // menu drop down selection BD
    $('.dropdown-btn').click(function (e) {
        e.preventDefault();
        $(this).next(':first').toggleClass('dropdown-collapse-show')
    })
    // fonction de filtrage
    $("#search-btn1").on("keyup", function () {
        let value = $(this).val().toLowerCase();
        $(".dropdown-content li").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
    $("#search-btn2").on("keyup", function () {
        let value = $(this).val().toLowerCase();
        $(".dropdown-content li").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
    $("#search-btn3").on("keyup", function () {
        let value = $(this).val().toLowerCase();
        $(".dropdown-content li").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

})(jQuery);


